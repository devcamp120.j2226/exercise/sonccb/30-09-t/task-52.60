import java.util.ArrayList;
import java.util.Date;
import com.devcamp.j03_javabasic.s50.Order;
import com.devcamp.j03_javabasic.s60.Order2;
import com.devcamp.javacore.Person;

public class App {
    public static void main(String[] args) throws Exception {
        // ArrayList<Order> arrayList = new ArrayList<>();
        ArrayList<Order2> arrayList2 = new ArrayList<>();
        //Khởi tạo order với các tham số khác nhau (khởi tạo 4 object Order)
        // Order Order1 = new Order();
        // Order Order2 = new Order("Sơn");
        // Order Order3 = new Order(3,"Lina", 80000);
        // Order Order4 = new Order(4,"Nam", 82000, new Date(), true ,new String[]{"Hộp màu" , "tay", "giấy màu"});
        // //Thêm object order vào danh sách
        // arrayList.add(Order1);
        // arrayList.add(Order2);
        // arrayList.add(Order3);
        // arrayList.add(Order4);
        //Khởi tạo order với các tham số khác nhau (khởi tạo 4 object Order)
        Order2 Order5 = new Order2();
        Order2 Order6 = new Order2("Sơn", new Person("Sơn",  29,  78, 1000000 , new String[] {"Bird", "Cat", "Dog"}));
        Order2 Order7 = new Order2(3,"Lina", 80000);
        Order2 Order8 = new Order2(4,"Nam", 82000, new Date(), true ,new String[]{"purse", "bag", "shoes"}, new Person("Sơn",  29,  78, 1000000 , new String[] {"chó ", "mèo", "gà"}));
        //Thêm object order vào danh sách
        arrayList2.add(Order5);
        arrayList2.add(Order6);
        arrayList2.add(Order7);
        arrayList2.add(Order8);

        //In ra màn hình
        // for (Order order : arrayList) {
        //     System.out.println(order.toString());
        // }
        //In ra màn hình
        for (Order2 order2 : arrayList2) {
            System.out.println(order2.toString());
        }
    }
}
