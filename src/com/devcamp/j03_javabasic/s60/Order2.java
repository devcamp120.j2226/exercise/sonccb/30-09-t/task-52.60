package com.devcamp.j03_javabasic.s60;
import java.util.Date;
import java.util.Locale;
import java.util.Arrays;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import com.devcamp.javacore.Person;

public class Order2 {
 int id; // id của order
 String customerName; // tên khách hàng
 Integer price;// tổng giá tiền
 Date orderDate; // ngày thực hiện order
 Boolean confirm; // đã xác nhận hay chưa?
 String[] items; // danh sách mặt hàng đã mua
 Person buyer;// người mua, là một object thuộc class Person
 public  Order2(String customerName,Person person) {
  this.id = 2;
  this.customerName = customerName;
  this.price = 1200000;
  this.orderDate = new Date();
  this.confirm = true; 
  this.items = new String[] {"purse", "bag", "shoes"};
  this.buyer = person;
 }
 //Khỏi tạo với tất cả tham số
 public  Order2(int id , String customerName, int price, Date orderDate, boolean confirm, String[] items, Person person) {
  this.id = id;
  this.customerName = customerName;
  this.price = price;
  this.orderDate = orderDate;
  this.confirm = confirm;
  this.items = new String[] {"purse", "bag", "shoes"};
  this.buyer = person;
}
//Khởi tạo không tham số
public Order2(){
   this("son", new Person());
}
//Khởi tạo với 3 tham số
 public Order2(int id, String customerName, int price) {
  this(id, customerName, price, new Date(), true, new String[] {"purse", "bag", "shoes"}, new Person());
}
@Override
 public String toString() {
  //Định dạng tiêu chuẩn Việt Nam
  Locale.setDefault(new Locale("vi","VN"));
  //Định dạng cho ngày tháng
  String pattern = "dd-MMMM-yyyy HH:mm:ss.SSS";
  DateTimeFormatter defaultTimeFomatter = DateTimeFormatter.ofPattern(pattern);
  //Định dạng cho giá tiền
  Locale usLocale = Locale.getDefault();
  NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
  //return (trả lại) chuỗi (String)
  return
  "Order [id= " + id
  + ", customerNeme= " + customerName
  + ", price= " + usNumberFormat.format(price)
  + ", orderDate= " + defaultTimeFomatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
  + ",confirm: "+ confirm
  + ", items= " + Arrays.toString(items)
  + ", buyerName= " + buyer.name
  + ", buyerAge= " + buyer.age
  + ", buyerWeight= " + buyer.weight
  + ", buyerSalary= " + buyer.salary
  + ", buyerPet= " + Arrays.toString(buyer.pets);
 }
}
